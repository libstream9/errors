#ifndef STREAM9_ERRORS_FIND_NESTED_ERROR_HPP
#define STREAM9_ERRORS_FIND_NESTED_ERROR_HPP

#include <concepts>
#include <exception>
#include <type_traits>

namespace stream9::errors {

template<typename T>
T const*
find_nested_error(std::exception_ptr const ptr = std::current_exception())
{
    try {
        std::rethrow_exception(ptr);
    }
    catch (T const& e) {
        return &e;
    }
    catch (std::nested_exception const& e) {
        return find_nested_error<T>(e.nested_ptr());
    }

    return nullptr;
}

template<typename T1, typename T2>
T1 const*
find_nested_error(T2 const& e)
{
    if constexpr (std::same_as<std::remove_cvref_t<T1>, std::remove_cvref_t<T2>>) {
        return &e;
    }

    try {
        std::rethrow_if_nested(e);
    }
    catch (T1 const& e) {
        return &e;
    }
    catch (std::nested_exception const& e) {
        return find_nested_error<T1>(e.nested_ptr());
    }

    return nullptr;
}

} // namespace stream9::errors

#endif // STREAM9_ERRORS_FIND_NESTED_ERROR_HPP
