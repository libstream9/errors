#ifndef STREAM9_ERRORS_PRINT_ERROR_HPP
#define STREAM9_ERRORS_PRINT_ERROR_HPP

#include "error.hpp"

#include <concepts>
#include <exception>
#include <iostream>

namespace stream9::errors {

void print_error(std::ostream&, std::exception_ptr);

template<typename T>
void
print_error(std::ostream& os, T const& e)
{
    print_error(os, std::make_exception_ptr(e));
}

inline void
print_error()
{
    print_error(std::cerr, std::current_exception());
}

inline void
print_error(std::ostream& os)
{
    print_error(os, std::current_exception());
}

inline std::ostream&
operator<<(std::ostream& os, error const& e)
{
    print_error(os, e);
    return os;
}

} // namespace stream9::errors

#endif // STREAM9_ERRORS_PRINT_ERROR_HPP
