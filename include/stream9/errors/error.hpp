#ifndef STREAM9_ERRORS_ERROR_HPP
#define STREAM9_ERRORS_ERROR_HPP

#include "error_code.hpp"
#include "namespace.hpp"

#include <exception>
#include <source_location>
#include <string>
#include <system_error>

namespace stream9::errors {

/*
 * @model std::copyable
 * @model std::equality_comparable
 */
class error : public std::exception
{
public:
    error(std::error_code why,
          std::source_location where = std::source_location::current()) noexcept;

    error(std::error_code why,
          std::string context,
          std::source_location where = std::source_location::current()) noexcept;

    error(std::string what,
          std::error_code why,
          std::source_location where = std::source_location::current()) noexcept;

    error(std::string what,
          std::error_code why,
          std::string context,
          std::source_location where = std::source_location::current()) noexcept;

    // accessor
    char const* what() const noexcept override;

    std::error_code const& why() const noexcept;

    std::source_location const& where() const noexcept;

    std::string const& context() const noexcept;

    // equality
    bool operator==(error const&) const = default;

private:
    std::string m_what;
    std::error_code m_why;
    std::string m_context;
    std::source_location m_where;
};

} // namespace stream9::errors

#endif // STREAM9_ERRORS_ERROR_HPP
