#ifndef STREAM9_ERRORS_INTERNAL_ERROR_HPP
#define STREAM9_ERRORS_INTERNAL_ERROR_HPP

#include "error.hpp"
#include "print_error.hpp"

#include <exception>
#include <iosfwd>
#include <source_location>
#include <string>

namespace stream9::errors {

class internal_error : public error
{
public:
    internal_error(std::source_location where = std::source_location::current()) noexcept
        : error { errc::internal_error, std::move(where) }
    {}

    internal_error(std::string context,
                   std::source_location where = std::source_location::current()) noexcept
        : error { errc::internal_error, std::move(context), std::move(where) }
    {}
};

// throw internal_error with current_exception nested
[[noreturn]] inline void
throw_with_internal_error(std::string cxt,
                     std::source_location loc = std::source_location::current())
{
    std::throw_with_nested(
        internal_error(std::move(cxt), std::move(loc)) );
}

[[noreturn]] inline void
throw_with_internal_error(std::source_location loc = std::source_location::current())
{
    throw_with_internal_error(std::string(), std::move(loc));
}

// print current_exception nested with internal_error
inline void
print_internal_error(std::ostream& os, std::string cxt,
                     std::source_location loc = std::source_location::current())
{
    try {
        throw_with_internal_error(std::move(cxt), std::move(loc));
    }
    catch (...) {
        print_error(os);
    }
}

inline void
print_internal_error(std::ostream& os,
                     std::source_location loc = std::source_location::current())
{
    print_internal_error(os, std::string(), std::move(loc));
}

} // namespace stream9::errors

#endif // STREAM9_ERRORS_INTERNAL_ERROR_HPP
