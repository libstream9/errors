#ifndef STREAM9_ERRORS_JSON_HPP
#define STREAM9_ERRORS_JSON_HPP

#include "error.hpp"

#include <exception>
#include <system_error>

#include <boost/core/demangle.hpp>

#include <stream9/json.hpp>

namespace stream9::json {

void tag_invoke(json::value_from_tag, json::value&, std::error_code const&);
void tag_invoke(json::value_from_tag, json::value&, std::exception const&);
void tag_invoke(json::value_from_tag, json::value&, std::exception_ptr const&);
void tag_invoke(json::value_from_tag, json::value&, std::system_error const&);

} // namespace stream9::json

namespace stream9::errors {

namespace json { using namespace stream9::json; }

inline void
assign_or_push_back(json::value& v1, json::value&& v2)
{
    if (v1.is_array()) {
        auto& a = v1.get_array();
        a.push_back(std::move(v2));
    }
    else {
        v1 = std::move(v2);
    }
}

inline void
push_back(json::value& v1, json::value&& v2)
{
    if (v1.is_array()) {
        auto& a = v1.get_array();
        a.push_back(std::move(v2));
    }
    else {
        auto& a = v1.emplace_array();
        a.push_back(std::move(v2));
    }
}

inline void
tag_invoke(json::value_from_tag t,
           json::value& v,
           error const& e) //TODO avoid conversion constructor
{
    json::object obj;
    auto& where = e.where();

    obj["what"] = e.what();
    obj["why"] = json::value_from(e.why());
    obj["where"] = json::object {
        { "filename", where.file_name() },
        { "function name", where.function_name() },
        { "line", where.line() },
        { "column", where.column() },
    };
    obj["context"] = e.context();

    if (auto* const ex = dynamic_cast<std::nested_exception const*>(&e)) {
        push_back(v, std::move(obj));

        tag_invoke(t, v, ex->nested_ptr());
    }
    else {
        assign_or_push_back(v, std::move(obj));
    }
}

} // namespace stream9::errors

namespace stream9::json {

inline void
tag_invoke(json::value_from_tag t,
           json::value& v,
           std::exception const& e)
{
    using stream9::errors::assign_or_push_back;
    using stream9::errors::push_back;

    json::object obj;

    obj["type"] = boost::core::demangle(typeid(e).name());
    obj["what"] = e.what();

    if (auto* const ex = dynamic_cast<std::nested_exception const*>(&e)) {
        push_back(v, std::move(obj));
        tag_invoke(t, v, ex->nested_ptr());
    }
    else {
        assign_or_push_back(v, std::move(obj));
    }
}

inline void
tag_invoke(json::value_from_tag t,
           json::value& v,
           std::system_error const& e)
{
    using stream9::errors::assign_or_push_back;
    using stream9::errors::push_back;

    json::object obj;

    obj["type"] = boost::core::demangle(typeid(e).name());
    obj["code"] = json::value_from(e.code());
    obj["message"] = e.code().message();
    obj["what"] = e.what();

    if (auto* const ex = dynamic_cast<std::nested_exception const*>(&e)) {
        push_back(v, std::move(obj));
        tag_invoke(t, v, ex->nested_ptr());
    }
    else {
        assign_or_push_back(v, std::move(obj));
    }
}

inline void
tag_invoke(json::value_from_tag t,
           json::value& v,
           std::exception_ptr const& ptr)
{
    using stream9::errors::assign_or_push_back;
    using stream9::errors::push_back;

    json::value v2;

    try {
        std::rethrow_exception(ptr);
    }
    catch (stream9::errors::error const& e) {
        tag_invoke(t, v, e);
    }
    catch (std::system_error const& e) {
        tag_invoke(t, v, e);
    }
    catch (std::exception const& e) {
        tag_invoke(t, v, e);
    }
    catch (std::nested_exception const& e) {
        json::object obj;

        obj["type"] = "unknown";

        push_back(v, std::move(obj));

        tag_invoke(t, v, e.nested_ptr());
    }
    catch (...) {
        json::object obj;

        obj["type"] = "unknown";

        assign_or_push_back(v, std::move(obj));
    }
}

inline void
tag_invoke(json::value_from_tag,
           json::value& v,
           std::error_code const& e)
{
    auto& obj = v.emplace_object();

    obj["value"] = e.value();
    obj["category"] = e.category().name();
    obj["message"] = e.message();
}

} // namespace stream9::json

#endif // STREAM9_ERRORS_JSON_HPP
