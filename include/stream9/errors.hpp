#ifndef STREAM9_ERRORS_HPP
#define STREAM9_ERRORS_HPP

#include "errors/error.hpp"
#include "errors/print_error.hpp"
#include "errors/internal_error.hpp"
#include "errors/current_exception_type.hpp"
#include "errors/exception_cast.hpp"
#include "errors/current_error_code.hpp"

#if (__has_include(<stream9/json.hpp>))

#include "errors/json.hpp"

#endif // __has_include(<stream9/json.hpp>)

#endif // STREAM9_ERRORS_HPP
