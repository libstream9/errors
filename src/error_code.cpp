#include <stream9/errors/error_code.hpp>

namespace stream9::errors {

std::error_category const&
error_category()
{
    static struct impl : std::error_category {
        char const* name() const noexcept
        {
            return "stream9::errors";
        }

        std::string message(int const condition) const
        {
            switch (static_cast<errc>(condition)) {
                using enum errc;
                case internal_error:
                    return "internal error";
            }

            return "unknown error";
        }
    } instance;

    return instance;
}

} // namespace stream9::errors
