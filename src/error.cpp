#include <stream9/errors/error.hpp>

#include <stream9/errors/namespace.hpp>

namespace stream9::errors {

error::
error(std::error_code why,
      std::source_location where) noexcept
    : m_why { std::move(why) }
    , m_where { std::move(where) }
{}

error::
error(std::error_code why,
      std::string context,
      std::source_location where) noexcept
    : m_why { std::move(why) }
    , m_context { std::move(context) }
    , m_where { std::move(where) }
{}

error::
error(std::string what,
      std::error_code why,
      std::source_location where) noexcept
    : m_what { std::move(what) }
    , m_why { std::move(why) }
    , m_where { std::move(where) }
{}

error::
error(std::string what,
      std::error_code why,
      std::string context,
      std::source_location where) noexcept
    : m_what { std::move(what) }
    , m_why { std::move(why) }
    , m_context { std::move(context) }
    , m_where { std::move(where) }
{}

char const* error::
what() const noexcept
{
    if (m_what.empty()) {
        return m_where.function_name();
    }
    else {
        return m_what.c_str();
    }
}

std::error_code const& error::
why() const noexcept
{
    return m_why;
}

std::source_location const& error::
where() const noexcept
{
    return m_where;
}

std::string const& error::
context() const noexcept
{
    return m_context;
}

} // namespace stream9::errors
