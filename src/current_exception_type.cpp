#include <stream9/errors/current_exception_type.hpp>

#include <cxxabi.h>

namespace stream9::errors {

std::type_info*
current_exception_type() noexcept
{
    return abi::__cxa_current_exception_type();
}

bool
is_current_exception(std::type_info const& ti) noexcept
{
    auto* const ci = current_exception_type();

    return ci ? *ci == ti : false;
}

} // namespace stream9::errors
