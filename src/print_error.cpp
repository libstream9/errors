#include <stream9/errors/print_error.hpp>

#include <stream9/errors/error.hpp>

#include <ostream>
#include <string>

namespace stream9::errors {

static std::string
make_padding(int const level)
{
    std::string result;

    if (level != 0) {
        result.push_back('\n');
        for (auto i = 0; i < level; ++i) {
            result.append("  ");
        }
    }

    return result;
}

static void
print_error_impl(std::ostream& os, std::exception_ptr const& ptr,
                 int level = 0)
{
    os << make_padding(level);

    try {
        std::rethrow_exception(ptr);
    }
    catch (error const& e) {
        auto const& where = e.where();
        auto const& context = e.context();

        os << where.file_name() << ':' << where.line()
           << ": " << e.what()
           << ": " << e.why().message()
           << ':';

        if (!context.empty()) {
            os << ' ' << context;
        }
    }
    catch (std::exception const& e) {
        os << e.what();
    }
    catch (std::string_view const& e) {
        os << e;
    }
    catch (std::string const& e) {
        os << e;
    }
    catch (char const* const e) {
        os << e;
    }
    catch (...) {
        os << "unknown error";
    }

    try {
        std::rethrow_exception(ptr);
    }
    catch (std::nested_exception const& e) {
        print_error_impl(os, e.nested_ptr(), level + 1);
    }
    catch (...)
    {}
}

void
print_error(std::ostream& os, std::exception_ptr const ptr)
{
    print_error_impl(os, ptr);
    os << std::endl;
}

} // namespace stream9::errors
